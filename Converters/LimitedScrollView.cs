namespace MobileProject.Converters;


    public class LimitedScrollView : ScrollView
    {
        public LimitedScrollView()
        {
            Scrolled += OnScrolled;
        }

        private void OnScrolled(object sender, ScrolledEventArgs e)
        {
            // Calculate the maximum vertical scroll position
            double maxVerticalScroll = Content.Height - Height;

            if (maxVerticalScroll <= 300)
                return;

            if (e.ScrollY < 0)
            {
                // Prevent scrolling beyond the top
                ScrollToAsync(0, 0, false);
            }
            else if (e.ScrollY > maxVerticalScroll)
            {
                // Prevent scrolling beyond the bottom
                ScrollToAsync(0, maxVerticalScroll, false);
            }
        }
    }

