using Microsoft.Maui.Controls;
using Mopups.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MobileProject.Views;

public partial class HomePageAfterLogin : ContentPage
{
    public ObservableCollection<ImageItem> ImageItems { get; } = new ObservableCollection<ImageItem>();
    public List<BulletPointItem> BulletPointItems { get; } = new List<BulletPointItem>();

    private int currentSectionIndex = 0; // Keeps track of the current section index
    private View[] sections; // Array to hold your section views

    private bool isButtonsFrameVisible = false; // Initialize as not visible
    public HomePageAfterLogin()
    {
        InitializeComponent();

        ProfilButton2_Clicked.Clicked += OnProfilButtonClicked;
        LogOutButton_Clicked.Clicked += OnLogOutButtonClicked;

        sections = new View[] { HomeSection2, SiapaKamiSection2, LihatJobSection2, SkillSection2 };

        ImageItems.Add(new ImageItem { ImageSource = "coding1.png", Label1 = "Website", Label2 = ".Net, PHP, Json, HTML" });
        ImageItems.Add(new ImageItem { ImageSource = "coding2.png", Label1 = "Mobile", Label2 = "Java, React, Kotlin" });
        ImageItems.Add(new ImageItem { ImageSource = "coding3.png", Label1 = "Robot Automation", Label2 = "RPA.." });
        ImageItems.Add(new ImageItem { ImageSource = "coding4.png", Label1 = "Testing", Label2 = "Automation, Manual" });

        BulletPointItems.AddRange(new[]
        {
                new BulletPointItem { Text = ".NET                                                 (1)" },
                new BulletPointItem { Text = "PHP                                                  (9)" },
                new BulletPointItem { Text = "Json                                                 (10)"},
                new BulletPointItem { Text = "HTML                                                 (3)" },
            });

        BindingContext = this;
    }

    private void OnSwipeUp(object sender, SwipedEventArgs e)
    {
        if (currentSectionIndex < sections.Length - 1)
        {
            currentSectionIndex++;
            ScrollToSection(currentSectionIndex);
        }
    }

    private void OnSwipeDown(object sender, SwipedEventArgs e)
    {
        if (currentSectionIndex > 0)
        {
            currentSectionIndex--;
            ScrollToSection(currentSectionIndex);
        }
    }

    private async void ScrollToSection(int index)
    {
        await scrollView3.ScrollToAsync(sections[index], ScrollToPosition.Start, true);
    }

   


    // Image Button Profile  and also navigation for Edit Profil and Log Out
    
    // Image Button Clicked Functionality
    private void OnProfilButtonClicked(object sender, EventArgs e)
    {
        // Toggle the visibility state
        isButtonsFrameVisible = !isButtonsFrameVisible;

        // Set the visibility of the ButtonsEditLogOut frame
        ButtonsEditLogOut.IsVisible = isButtonsFrameVisible;
    }

    // Navigation Edit Profile


    // Navigation Log Out
    private async void OnLogOutButtonClicked(object sender, EventArgs e)
    {
        await Navigation.PushAsync(new HomePage());
    }



    // Navigation Join Member Button (Register Page)
    private async void RegisterButton_Clicked(object sender, EventArgs e)
    {
        await Navigation.PushAsync(new RegisterPage());
    }

    // Navigation Scroll for section siapa kami
    private async void SiapaKamiButton_Clicked(object sender, EventArgs e)
    {
        await scrollView3.ScrollToAsync(SiapaKamiSection2, ScrollToPosition.Start, true);
    }

    private async void LihatJobButton_Clicked(object sender, EventArgs e)
    {
        await scrollView3.ScrollToAsync(LihatJobSection2, ScrollToPosition.Start, true);
    }

    private async void ArrowButton_Clicked(object sender, EventArgs e)
    {
        await scrollView3.ScrollToAsync(HomeSection2, ScrollToPosition.Start, true);
    }

    private async void ImageButton_Clicked(object sender, EventArgs e)
    {
        await scrollView3.ScrollToAsync(SkillSection2, ScrollToPosition.Start, true);
    }


}

public class ImageItem1
{
    public string ImageSource { get; set; }
    public string Label1 { get; set; }
    public string Label2 { get; set; }
}

public class BulletPointItem2
{
    public string Text { get; set; }
}
