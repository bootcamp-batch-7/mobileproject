namespace MobileProject.Views;

public partial class OTP : ContentPage
{
	public OTP()
	{
		InitializeComponent();
	}

    private void OnEntry1TextChanged(object sender, TextChangedEventArgs e)
    {
        if (e.NewTextValue.Length >= 1)
        {
            OTP2_Entry.Focus();
        }
    }

    private void OnEntry2TextChanged(object sender, TextChangedEventArgs e)
    {
        if (e.NewTextValue.Length >= 1)
        {
            OTP3_Entry.Focus();
        }
        else if (e.OldTextValue.Length == 1)
        {
            OTP1_Entry.Focus();
        }
    }

    private void OnEntry3TextChanged(object sender, TextChangedEventArgs e)
    {
        if (e.NewTextValue.Length >= 1)
        {
            OTP4_Entry.Focus();
        }
        else if (e.OldTextValue.Length == 1)
        {
            OTP2_Entry.Focus();
        }
    }

    private void OnEntry4TextChanged(object sender, TextChangedEventArgs e)
    {
        if (e.NewTextValue.Length == 0)
        {
            OTP3_Entry.Focus();
        }
    }

    private async void OTPSend_Clicked(object sender, EventArgs e)
    {
        await Navigation.PushAsync(new Repassword());
    }
}