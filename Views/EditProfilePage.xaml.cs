using Mopups.Services;

namespace MobileProject.Views;

public partial class EditProfilePage : ContentPage
{
	public EditProfilePage()
	{
		InitializeComponent();
	}

    private async void ConfirmButton_Clicked(object sender, EventArgs e)
    {
        await Navigation.PushAsync(new HomePageAfterLogin());
    }
}