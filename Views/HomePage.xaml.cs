using Microsoft.Maui.Controls;
using Mopups.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MobileProject.Views
{
    public partial class HomePage : ContentPage
    {
        public ObservableCollection<ImageItem> ImageItems { get; } = new ObservableCollection<ImageItem>();
        public List<BulletPointItem> BulletPointItems { get; } = new List<BulletPointItem>();

        private int currentSectionIndex = 0; // Keeps track of the current section index
        private View[] sections; // Array to hold your section views


        public HomePage()
        {
            InitializeComponent();
            sections = new View[] { HomeSection, SiapaKamiSection, LihatJobSection, SkillSection };

            ImageItems.Add(new ImageItem { ImageSource = "coding1.png", Label1 = "Website", Label2 = ".Net, PHP, Json, HTML" });
            ImageItems.Add(new ImageItem { ImageSource = "coding2.png", Label1 = "Mobile", Label2 = "Java, React, Kotlin" });
            ImageItems.Add(new ImageItem { ImageSource = "coding3.png", Label1 = "Robot Automation", Label2 = "RPA.." });
            ImageItems.Add(new ImageItem { ImageSource = "coding4.png", Label1 = "Testing", Label2 = "Automation, Manual" });

            BulletPointItems.AddRange(new[]
            {
                new BulletPointItem { Text = ".NET                                                 (1)" },
                new BulletPointItem { Text = "PHP                                                  (9)" },
                new BulletPointItem { Text = "Json                                                 (10)" },
                new BulletPointItem { Text = "HTML                                                 (3)" }
            });

            BindingContext = this;
        }

        private void OnSwipeUp(object sender, SwipedEventArgs e)
        {
            if (currentSectionIndex < sections.Length - 1)
            {
                currentSectionIndex++;
                ScrollToSection(currentSectionIndex);
            }
        }

        private void OnSwipeDown(object sender, SwipedEventArgs e)
        {
            if (currentSectionIndex > 0)
            {
                currentSectionIndex--;
                ScrollToSection(currentSectionIndex);
            }
        }

        private async void ScrollToSection(int index)
        {
            await scrollView.ScrollToAsync(sections[index], ScrollToPosition.Start, true);
        }

        private void LoginButton_Clicked(object sender, EventArgs e)
        {
            MopupService.Instance.PushAsync(new FormLogin());
        }

        private async void RegisterButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RegisterPage());
        }


        private async void SiapaKamiButton_Clicked(object sender, EventArgs e)
        {
            await scrollView.ScrollToAsync(SiapaKamiSection, ScrollToPosition.Start, true);
        }

        private async void LihatJobButton_Clicked(object sender, EventArgs e)
        {
            await scrollView.ScrollToAsync(LihatJobSection, ScrollToPosition.Start, true);
        }

        private async void ArrowButton_Clicked(object sender, EventArgs e)
        {
            await scrollView.ScrollToAsync(HomeSection, ScrollToPosition.Start, true);
        }

        private async void ImageButton_Clicked(object sender, EventArgs e)
        {
            await scrollView.ScrollToAsync(SkillSection, ScrollToPosition.Start, true);
        }

        private async void Webstie_Tapped(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ListProject());
        }

       





    }

    public class ImageItem
    {
        public string ImageSource { get; set; }
        public string Label1 { get; set; }
        public string Label2 { get; set; }
    }

    public class BulletPointItem
    {
        public string Text { get; set; }
    }
}


