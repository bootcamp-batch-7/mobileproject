namespace MobileProject.Views;

public partial class ForgotPassword : ContentPage
{
	public ForgotPassword()
	{
		InitializeComponent();
	}

    private async void SendEmail_Clicked(object sender, EventArgs e)
    {
        await Navigation.PushAsync(new OTP());
    }
}