using Microsoft.Maui.Controls;
using System;
using System.IO;
using System.Threading.Tasks;
using Mopups.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;


namespace MobileProject.Views;


public partial class RegisterPage : ContentPage



{
    // get set for 
    public string Keyword { get; set; }
    public string PickerValue { get; set; }

    public string Name  { get; set; }
    public string Email { get; set; }

    public string NoTelp { get; set; }
    public string Password { get; set; }




    private List<string> keywords = new List<string>
    {
        ".NET", "PHP", "JSON", "HTML", "Java" , "React" , "Kotlin", "RPA" , "Automation"
    };

    private HashSet<string> clickedKeywords = new HashSet<string>();

    



    public RegisterPage()
    {
        InitializeComponent();
        BindingContext = this;

        // Initialize event handlers

        myScrollView.IsEnabled = false;


        
    }


    private void SkillPageFrame_Tapped(object sender, EventArgs e)
    {
        // Disable myScrollView when SkillPageFrame is tapped
        myScrollView.IsEnabled = false;
    }








    // For Creating the Search Bar and Frame SkillSET Functionality


    private void OnSearchTextChanged(object sender, TextChangedEventArgs e)
    {
        string searchText = e.NewTextValue;
        var matchingKeywords = keywords.FindAll(keyword => keyword.Contains(searchText, StringComparison.OrdinalIgnoreCase));
        searchResultsListView.ItemsSource = matchingKeywords;
        searchResultsListView.IsVisible = matchingKeywords.Count > 0;
        myScrollView.IsEnabled = true;
        
    }

    private void OnKeywordSelected(object sender, SelectedItemChangedEventArgs e)
    {
        if (e.SelectedItem is string selectedKeyword && !clickedKeywords.Contains(selectedKeyword))
        {
            // Create a new Frame
            var frame = new Frame
            {
                WidthRequest = 350,
                HeightRequest = 90,
                BackgroundColor = Color.FromHex("#FFFFFF"),
                CornerRadius = 10,
                HasShadow = true,
                HorizontalOptions = LayoutOptions.Start,
                Margin = new Thickness(20, 5),
                Content = new StackLayout
                {
                    Children =
                    {
                       

                        new Label
                        {
                            Text = selectedKeyword,
                            FontSize = 14,
                            FontAttributes = FontAttributes.Bold
                        },
                        new Frame
                        {
                            
                            WidthRequest = 200,
                            HeightRequest = 19,
                            HorizontalOptions = LayoutOptions.Start,
                            CornerRadius = 5,
                            Margin = new Thickness(0, 10),
                            Content = new StackLayout
                            {
                                WidthRequest = 200,
                                HeightRequest = 40,
                                Children =
                                {
                                    new Picker
                                    {
                                        FontSize = 10,
                                        ItemsSource = new List<string>
                                        {
                                            "1 - Beginner",
                                            "2 - Competent",
                                            "3 - Proficient",
                                            "4 - Expert",
                                            "5 - Master"
                                        }
                                    }
                                }
                            }
                        },

                  
                    }
                }

                
            };

            // Add the Frame to the container
            framesContainer.Children.Add(frame);

            // Add the clicked keyword to the HashSet
            clickedKeywords.Add(selectedKeyword);

            // Clear the search bar text and hide the ListView
            searchBar.Text = string.Empty;
            searchResultsListView.IsVisible = false;

            // Calculate the total height of the content in the StackLayout
            double totalHeight = framesContainer.Children.Sum(child => child.Height);

            // Set the MinimumHeightRequest of the ScrollView
            myScrollView.MinimumHeightRequest = totalHeight;


            

        }

        
    }

    private void SkillPageTapped(object sender, EventArgs e)
    {
        // Disable myScrollView when SkillPageSection is tapped
        myScrollView.IsEnabled = false;
    }




    private async void Register_Clicked(object sender, EventArgs e)
    {

        
        await Navigation.PushAsync(new JoinMember());
        
    }

    // Functionality Button Scroll to ProfileDetail

    private async void Next_Clicked(object sender, EventArgs e)
    {
        myScrollView.IsEnabled = false;
        await scrollView2.ScrollToAsync(ProfilDetailSection, ScrollToPosition.Start, true);
    }


    // Functionality Button Scroll to Skill Page Section
    private async void Back2_Clicked(object sender, EventArgs e)
    {
        await scrollView2.ScrollToAsync(SkillPageSection, ScrollToPosition.Start, true);
    }

    private void SkillPickerSelectedIndexChangedHandlerNet(object sender, EventArgs e)
    {
        if (sender is Picker picker && picker.SelectedIndex >= 0)
        {
            string 
                
                
                
                selectedSkillnet = (string)picker.SelectedItem;
            
        }
    }

    private void SkillPickerSelectedIndexChangedHandlerPython(object sender, EventArgs e)
    {
        if (sender is Picker picker && picker.SelectedIndex >= 0)
        {
            string selectedSkillpython = (string)picker.SelectedItem;

        }
    }

    // Image Control






    private async void OnPickImageClicked(object sender, EventArgs e)
    {
        try
        {
            var result = await FilePicker.PickAsync(new PickOptions
            {
                PickerTitle = "Pick Image Please",
                FileTypes = FilePickerFileType.Images
            });

            if (result == null)
                return;

            var stream = await result.OpenReadAsync();

            // Get the original file name from the result object
            string originalFileName = result.FileName;

            // Define the folder name
            string folderName = "imagesaver";

            // Get the path to the "imagesaver" folder within the app's data directory
            string appDataDirectory = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            string folderPath = Path.Combine(appDataDirectory, folderName);

            // Create the folder if it doesn't exist
            Directory.CreateDirectory(folderPath);

            // Combine the folder path with the original file name
            string filePath = Path.Combine(folderPath, originalFileName);

            // Create or overwrite the file with the image data
            using (FileStream fs = new FileStream(filePath, FileMode.Create))
            {
                await stream.CopyToAsync(fs);
            }

            // Set the image source to the saved file
            PickedImage.Source = ImageSource.FromFile(filePath);
        }
        catch (Exception ex)
        {
            // Handle exceptions, e.g., file I/O errors
            Console.WriteLine($"Error: {ex.Message}");
        }
    }




    // Datetime Selector

    private DateTime selectedDate;

    public DateTime SelectedDate
    {
        get { return selectedDate; }
        set
        {
            if (selectedDate != value)
            {
                selectedDate = value;
                OnPropertyChanged(nameof(SelectedDate));
            }
        }
    }




    private async void PickImages_Clicked(object sender, EventArgs e)
    {
        // For custom file types            
        //var customFileType =
        //	new FilePickerFileType(new Dictionary<DevicePlatform, IEnumerable<string>>
        //	{
        //		 { DevicePlatform.iOS, new[] { "com.adobe.pdf" } }, // or general UTType values
        //       { DevicePlatform.Android, new[] { "application/pdf" } },
        //		 { DevicePlatform.WinUI, new[] { ".pdf" } },
        //		 { DevicePlatform.Tizen, new[] { "*/*" } },
        //		 { DevicePlatform.macOS, new[] { "pdf"} }, // or general UTType values
        //	});

        var results = await FilePicker.PickMultipleAsync(new PickOptions
        {
            //FileTypes = customFileType
        });

        foreach (var result in results)
        {
            await DisplayAlert("You picked...", result.FileName, "OK");
        }
    }



    private string _displayText = "Upload CV";
    private string uploadedCVFilePath; // Store the path to the uploaded CV file here
 


    public string DisplayText
    {
        get => _displayText;
        set
        {
            if (_displayText != value)
            {
                _displayText = value;
                OnPropertyChanged(nameof(DisplayText));
            }
        }
    }

    private async void OnUploadDownloadCVLabelTapped(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(uploadedCVFilePath))
        {
            var action = await DisplayActionSheet("Opsi", "Cancel", null, "Download", "Delete");

            switch (action)
            {
                case "Download":
                    // Open the file for download
                    try
                    {
                        await Launcher.OpenAsync(new OpenFileRequest
                        {
                            File = new ReadOnlyFile(uploadedCVFilePath)
                        });
                    }
                    catch (Exception ex)
                    {
                        await DisplayAlert("Error", $"An error occurred: {ex.Message}", "OK");
                    }
                    break;

                case "Delete":
                    // Delete the uploaded file
                    try
                    {
                        File.Delete(uploadedCVFilePath);
                        uploadedCVFilePath = null;
                        DisplayText = "Upload CV";
                    }
                    catch (Exception ex)
                    {
                        await DisplayAlert("Error", $"An error occurred: {ex.Message}", "OK");
                    }
                    break;
            }
        }
        else
        {
            // Upload a file
            try
            {
                var file = await FilePicker.PickAsync(new PickOptions
                {
                    FileTypes = FilePickerFileType.Pdf
                });

                if (file != null)
                {
                    uploadedCVFilePath = file.FullPath;
                    DisplayText = System.IO.Path.GetFileName(uploadedCVFilePath);
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", $"An error occurred: {ex.Message}", "OK");
            }
        }
    }

    private void FinishButton_Clicked(object sender, EventArgs e)

    {

        String EnteredEmail = Email;

        if (EnteredEmail == "techno")
        {
            MopupService.Instance.PushAsync(new EmailAlreadyUsedPopUp());
        }

        else
        {
            MopupService.Instance.PushAsync(new FinishRegister());
        }
        
    }
}






