namespace MobileProject.Views;

using Mopups.Services;



public partial class FormLogin
{
    public string Email { get; set; } 
    public string Password { get; set; }
    
    public FormLogin()
    {
        InitializeComponent();
        BindingContext = this;
    }

    private async void LoginButton_Clicked(object sender, EventArgs e)
    {
        String EnteredEmail = Email;
        String EnteredPassword = Password;
       

        if (EnteredEmail=="Minum")
        {
            
            MopupService.Instance.PushAsync(new EmailUsedPopUp());




        }
        else if (EnteredPassword=="Makan")
        {
            MopupService.Instance.PushAsync(new WrongPasswordPopUp());
        }

        else

        {
            MopupService.Instance.PopAsync();
            await Navigation.PushAsync(new HomePageAfterLogin());
        }

    }


    private async void ForgotPassword_Tapped(object sender, EventArgs e)
    {
        MopupService.Instance.PopAsync();
        await Navigation.PushAsync(new ForgotPassword());

    }

    private async void JoinMemberLabel_Tapped(object sender, EventArgs e)
    {
        MopupService.Instance.PopAsync();
        await Navigation.PushAsync(new RegisterPage());

    }


}