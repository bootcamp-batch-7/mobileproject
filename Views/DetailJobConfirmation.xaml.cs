using Mopups.Services;

namespace MobileProject.Views;

public partial class DetailJobConfirmation
{
	public DetailJobConfirmation()
	{
		InitializeComponent();
	}

    private void Confirmation_Clicked(object sender, EventArgs e)
    {
        MopupService.Instance.PushAsync(new CongratulationPopUp());
    }

}