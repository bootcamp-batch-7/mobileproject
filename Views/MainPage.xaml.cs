﻿using System.Collections.ObjectModel;
using System.Net.Http.Json;
using System.Text.Json.Serialization;

namespace MobileProject.Views;

public partial class MainPage : ContentPage
{
    private readonly HttpClient httpClient = new();

    public bool IsRefreshing { get; set; }
    public ObservableCollection<Project> Projects { get; set; } = new();
    public Command RefreshCommand { get; set; }
    public Project SelectedProject { get; set; }

    public MainPage()
    {
        RefreshCommand = new Command(async () =>
        {
            // Simulate delay
            await Task.Delay(2000);

            await LoadProjects();

            IsRefreshing = false;
            OnPropertyChanged(nameof(IsRefreshing));
        });

        BindingContext = this;

        InitializeComponent();
    }

    protected async override void OnNavigatedTo(NavigatedToEventArgs args)
    {
        base.OnNavigatedTo(args);

        await LoadProjects();
    }

    private void Button_Clicked(object sender, EventArgs e)
    {
        Projects.Clear();
    }

    private async Task LoadProjects()
    {
        var projects = await httpClient.GetFromJsonAsync<Project[]>("https://gitlab.com/DimaKeda/MAUI/-/raw/master/projectdata.json?ref_type=heads");

        Projects.Clear();

        foreach (Project project in projects)
        {
            Projects.Add(project);
        }
    }

    private void OnCounterClicked(object sender, EventArgs e)
    {
        App.Current.MainPage = new NavigationPage(new JoinMember());

       /* SemanticScreenReader.Announce(CounterBtn.Text);*/
    }
}

