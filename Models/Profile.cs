namespace MobileProject.Models;

public class UserProfile
{
    public string Name { get; set; }
    public DateTime BirthDate { get; set; }
    public string Email { get; set; }
    public string NoTelp { get; set; }
    public string Password { get; set; }
    // Add other properties as needed
}