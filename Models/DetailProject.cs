﻿namespace MobileProject
{
    public class DetailProject
    {
        public string Framework { get; set; }
        public int Estimation { get; set; }
        public string Satuan { get; set; }
        public int Harga { get; set; }
        public int Id { get; set; }
    }
}
